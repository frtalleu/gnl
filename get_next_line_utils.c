/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/23 03:58:37 by frtalleu          #+#    #+#             */
/*   Updated: 2019/11/28 02:22:24 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <unistd.h>
#include "get_next_line.h"
#include <stdlib.h>

int		ft_checkline(char *str)
{
	int				i;

	i = 0;
	if (!str || str[i] == '\0')
		return (-1);
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			return (i);
		i++;
	}
	return (-1);
}

size_t	ft_strlen(const char *s)
{
	size_t			i;

	i = 0;
	if (!s)
		return (0);
	while (s[i] != '\0')
		i++;
	return (i);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	i;
	char			*j;
	char			*tab;

	if (!s1 && !s2)
		return (0);
	if (!s1)
		return (ft_strdup((char *)s2));
	if (!s2)
		return (ft_strdup((char *)s1));
	i = ft_strlen(s1) + ft_strlen(s2);
	if (!(tab = malloc((i + 1) * (sizeof(char)))))
		return (NULL);
	j = tab;
	while (*s1 != '\0')
		*j++ = *s1++;
	while (*s2 != '\0')
		*j++ = *s2++;
	*j = '\0';
	return (tab);
}

char	*ft_strdup(const char *src)
{
	int				i;
	char			*c;

	c = NULL;
	i = 0;
	if (!src)
		return (ft_strdup(""));
	if (!(c = (char *)malloc(ft_strlen(src) * sizeof(char) + 1)))
		return (NULL);
	while (src[i] != '\0')
	{
		c[i] = src[i];
		i++;
	}
	c[i] = '\0';
	return (c);
}

t_char	*ft_cut(char *str)
{
	t_char			*res;
	int				i;

	if (!(res = malloc(sizeof(t_char))))
		return (NULL);
	i = ft_checkline(str);
	str[i] = '\0';
	if (!(res->lin = ft_strdup(str)))
		return (NULL);
	i++;
	if (!(res->stat = ft_strdup(&str[i])))
		return (NULL);
	free(str);
	return (res);
}
