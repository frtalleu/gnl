/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/24 05:51:11 by frtalleu          #+#    #+#             */
/*   Updated: 2019/11/27 02:16:59 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include <stddef.h>

typedef struct	s_char
{
	char	*lin;
	char	*stat;
}				t_char;

int				ft_checkline(char *str);
size_t			ft_strlen(const char *s);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strdup(const char *src);
t_char			*ft_cut(char *str);
int				get_next_line(int fd, char **line);
char			*ft_read(int fd, char **rest);
#endif
