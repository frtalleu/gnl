/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/24 03:57:23 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/12 04:23:29 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "get_next_line.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

char	*ft_read(int fd, char **rest)
{
	char			*buff;
	int				size;
	char			*tmp;

	size = BUFFER_SIZE;
	if (!(buff = malloc(sizeof(char) * (BUFFER_SIZE + 1))))
		return (NULL);
	while (size == BUFFER_SIZE && ft_checkline(*rest) == -1)
	{
		size = read(fd, buff, BUFFER_SIZE);
		buff[size] = '\0';
		if (!(tmp = ft_strjoin(*rest, buff)))
			return (NULL);
		if (*rest != 0)
			free(*rest);
		if (!(*rest = ft_strdup(tmp)))
			return (NULL);
		free(tmp);
	}
	free(buff);
	return (*rest);
}

int		get_next_line(int fd, char **line)
{
	static char *rest = NULL;
	t_char		*st;

	if (line)
		*line = NULL;
	if (fd < 0 || read(fd, NULL, 0) == -1 || BUFFER_SIZE <= 0
		|| !(rest = ft_read(fd, &rest)) || !line)
		return (-1);
	if (ft_checkline(rest) == -1)
	{
		*line = rest;
		rest = NULL;
		return (0);
	}
	if (!(st = ft_cut(rest)))
		return (-1);
	if (!(st->lin))
		return (-1);
	*line = st->lin;
	rest = st->stat;
	free(st);
	return (1);
}

int main(int ac, char **av)
{
	char *line;
	int fd;
	int gnl;
	int i = 1;
	(void)ac;
	fd = open(av[1], O_RDONLY);
	while ((gnl = get_next_line(fd, &line)) == 1)
	{
		printf("%d|%d|%s|\n", gnl, i, line);
		i++;
		free(line);
	}
	printf("%d|%s|\n", gnl, line);
	free(line);
	fd = open(av[2], O_RDONLY);
	while ((gnl = get_next_line(fd, &line)) == 1)
	{
		printf("%d|%s|\n", gnl, line);
		free(line);
	}
	printf("%d|%s|\n", gnl, line);
	free(line);
}
